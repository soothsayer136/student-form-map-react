import React, { Component } from 'react';
import Table from './Table ';

class DisplayComponents extends Component {
    handleStudent(name) {
        this.props.removeRow(name)
    }

    render() {
        return (
            <div>
                <table style={{width:600}}>
                    <thead>
                        <th>Name</th>
                        <th>Roll No</th>
                         <th>Class</th>
                        <th>Date of Birth</th>
                        <th>Email</th>
                    </thead>
                    <tbody>
                    {this.props.studentInfo.map((student, index) => {
                            return <Table key={index} tableData ={student} 
                                removeRow = {this.handleStudent.bind(this)}
                            />
                    })}
                        
                    </tbody>
                </table>
    

                
            </div>
        );
    }
}

export default DisplayComponents;