import React, { Component } from 'react';

class Table  extends Component {
    removeStudent(name) {
        this.props.removeRow(name)
    }

    render() {
        return (
            
                <tr>
                    <td>{this.props.tableData.name}</td>
                    <td>{this.props.tableData.roll}</td>
                    <td>{this.props.tableData.class}</td>
                    <td>{this.props.tableData.dob}</td>
                    <td>{this.props.tableData.email}</td>
                    <td><button type="button" class="btn btn-danger" onClick = {() => this.removeStudent(this.props.tableData.name)}>Remove</button></td>
                </tr>
            
        );
    }
}

export default Table ;