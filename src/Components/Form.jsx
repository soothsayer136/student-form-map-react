import React, { Component } from 'react';
import DisplayComponents from './DisplayComponents';

class Form extends Component {
    state ={
        studentData:{
            id:1,
            student_name:"",
            student_roll:"",
            student_email:"",
            student_class:"",
            student_address:"",
            student_dob:"",
            student_gender:"",
            // student_section:""
        },
        data:[] ,
        validationMessageForName:""
    }

    // handleClick(e) {
    //     let{name} = e.target;
    //     let{value} =e.target;
    //     if(name === "student_section"){
    //         this.setState({
    //             student_section:value
    //         })
    //     }
    // }

    handleChange(e) {
        let{name} = e.target;
        let{value} =e.target;
        let {studentData} = this.state;
        studentData[name] = value;
        this.setState({
            studentData
        })
    }

    handleSubmit(){
        let {data, studentData} = this.state;
        if (studentData.student_name=== ""){
            this.setState({
                validationMessageForName:"Name is required"
            })
        } else {
            data.push({
                name:studentData.student_name,
                roll:studentData.student_roll,
                class:studentData.student_class,
                dob:studentData.student_dob,
                email:studentData.student_email,
                gender:studentData.student_gender,
                // section:studentData.student_section
               
            })

            this.setState({
                data,
                student_name:"",
                student_roll:"",
                student_dob:"",
                student_class:"",
                student_email:"",
                student_gender: "",
                // student_section:""
            })
        }
    }

    handleRemove =(name) => {
        const data = this.state.data.filter(student => student.name !== name)
        this.setState({
            data:data
        })
    }

    showData(){
      return  <DisplayComponents
            studentInfo = {this.state.data}
            removeRow = {this.handleRemove.bind(this)}
        />
    }

   
    render() {
        return (
            <div style={{display:"flex", flexDirection:"column", alignItems:"center"}}>
                <h1>Student Form</h1>
                <div id="form" style= {{width:600}} >
                    <div className="mb-3" id='student_name'>
                        <label>Name</label>
                        <input type="text" className="form-control"  
                            name="student_name"
                            value={this.state.studentData.student_name}
                            onChange={(e) => this.handleChange(e)}
                        /><p style={{color:"Red"}}>{this.state.validationMessageForName}</p>
                    </div>
                    <div className="mb-3" id="student_roll">
                        <label>Roll No</label>
                        <input type="number" className="form-control"  
                            name="student_roll"
                            value={this.state.studentData.student_roll}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div className="mb-3" id='student_class'>
                        <label>Class</label>
                        <input type="number" className="form-control"  
                            name="student_class"
                            value={this.state.studentData.student_class}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div className="mb-3" id='student_gender'>
                        <label>Gender</label>
                        <input type="text" className="form-control"  
                            name="student_gender"
                            value={this.state.studentData.student_gender}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div className="mb-3" id='student_dob'>
                        <label>Date of Birth</label>
                        <input type="date" className="form-control"  
                            name="student_dob"
                            value={this.state.studentData.student_dob}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                   
                    <label for="sectionList" className="form-label">Section</label>
                        <input className="form-control" list="sectionOption" id="sectionList"
                        name="student_section"
                         value={this.state.studentData.student_section}
                            onClick={(e) => this.handleClick(e)}
                        />
                        <datalist id="sectionOption">
                            <option value="Section A"/>
                            <option value="Section B"/>
                            <option value="Section C"/>
                        </datalist>
                    <div className="mb-3">
                        <label>Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" 
                            name="student_email"
                            value={this.state.studentData.student_email}
                            onChange={(e)=> this.handleChange(e)}
                        />
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
                    <h1>Student Data</h1>
                    {this.showData()}
                </div>
            </div>
        );
    }
}

export default Form;